# Introduction of the project
We will walk through a real-world python machine learning project using the sci-kit learn package. 
In this project, we will create a model that automatically classifies text as having a positive or negative or neutral emotion. We accomplish this by using Amazon reviews as training data.
We will apply it for Books reviews, you can use it for any other article's review

# Tools and librairies used
- Python
- Pandas
- Json
- matplotlib
- random
- sklearn (model_selection, feature_extraction.text, svm, DecisionTreeClassifier, GaussianNB, LogisticRegression)
- sklearn.metrics (f1_score)

# Link for the dataset

http://jmcauley.ucsd.edu/data/amazon/